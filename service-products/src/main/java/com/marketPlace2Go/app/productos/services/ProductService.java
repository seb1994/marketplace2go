package com.marketPlace2Go.app.productos.services;

import com.marketPlace2Go.app.productos.model.entity.ProductTransaction;
import com.marketPlace2Go.app.productos.model.entity.Producto;
import com.marketPlace2Go.app.productos.model.repository.ProductTransactionRepository;
import com.marketplacego.service.commons.dto.OrderRequestDto;
import com.marketplacego.service.commons.dto.ProductRequestDto;
import com.marketplacego.service.commons.event.OrderEvent;
import com.marketplacego.service.commons.event.ProductEvent;
import com.marketplacego.service.commons.model.PurchaseOrderDetailDto;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.StockStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ProductService {

	@Autowired
	private  ProductTransactionRepository productTransactionRepository;

	@Autowired
	ProductoSrv productoSrv;


	@Transactional
	public ProductEvent newOrderEvent(OrderEvent orderEvent){
		List<PurchaseOrderDetailDto> productosList = new ArrayList<>();
		Producto producto = new Producto();
		ProductRequestDto productRequestDto = new ProductRequestDto();
		try{
		Thread.sleep(2000);
		OrderRequestDto orderRequestDto = orderEvent.getOrderRequestDto();
			productosList = orderEvent.getOrderRequestDto().getOrderDetails();
			for(PurchaseOrderDetailDto purchaseOrderDetailDto : productosList) {

				ProductTransaction productTransaction = new ProductTransaction();
				productTransaction.setOrderId(orderRequestDto.getId());
				productTransaction.setProductId(purchaseOrderDetailDto.getProductId());
				productTransaction.setCantidad(purchaseOrderDetailDto.getQuantity().intValue());
				productTransaction.setPrecio(purchaseOrderDetailDto.getPrice().longValue());
				productTransaction.setEstado(OrderStatus.ORDER_PAYMENT_PENDING);
				productTransaction.setStockStatus(StockStatus.STOCK_COMPLETED);

				Optional<Producto> productoDB = productoSrv.findById(purchaseOrderDetailDto.getProductId());
				if(null != productoDB){
					if(productoDB.get().getCantidad() - purchaseOrderDetailDto.getQuantity().intValue() >= 0) {
						productoDB.get().setCantidad((productoDB.get().getCantidad() - purchaseOrderDetailDto.getQuantity().intValue()));

					}
					else
						productTransaction.setStockStatus(StockStatus.STOCK_FAILED);
					productoSrv.save(productoDB.get());
				}
				productTransactionRepository.save(productTransaction);
			}

			return new ProductEvent(productRequestDto, OrderStatus.ORDER_CREATED);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;

	}


	@Transactional
	public void cancelOrderEvent(OrderEvent orderEvent) {

		productTransactionRepository.findById(orderEvent.getOrderRequestDto().getId()).ifPresent(ut -> {
			productTransactionRepository.delete(ut);
			productTransactionRepository.findById(ut.getOrderId())
					.ifPresent(ub -> {
						              ub.setCantidad(ub.getCantidad() - ut.getCantidad());
								      ub.setEstado(OrderStatus.ORDER_PAYMENTS_PROBLEMS);
							}

					           );
		});
	}


	private ProductRequestDto convertDtoToEntity(ProductTransaction productTransaction) {

		ProductRequestDto productRequestDto = new ProductRequestDto();
		productRequestDto.setOrderId(productTransaction.getOrderId());
		productRequestDto.setProductId(productTransaction.getProductId());
		productRequestDto.setPrecio(productTransaction.getPrecio());
		productRequestDto.setCantidad(productTransaction.getCantidad());

		return productRequestDto;
	}


}
