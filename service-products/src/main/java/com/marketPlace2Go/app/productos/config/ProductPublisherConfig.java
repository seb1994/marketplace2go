package com.marketPlace2Go.app.productos.config;

import com.marketplacego.service.commons.event.ProductEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import java.util.function.Supplier;

@Configuration
public class ProductPublisherConfig {

    @Bean
    public Sinks.Many<ProductEvent> productSinks(){
        return Sinks.many().multicast().onBackpressureBuffer();
    }

    @Bean
    public Supplier<Flux<ProductEvent>> productSupplier(Sinks.Many<ProductEvent> sinks){
       return sinks::asFlux;
    }
}
