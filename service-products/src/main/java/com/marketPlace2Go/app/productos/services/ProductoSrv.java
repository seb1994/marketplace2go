package com.marketPlace2Go.app.productos.services;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.marketPlace2Go.app.productos.model.entity.Producto;

public interface ProductoSrv  {
	
    public Iterable<Producto> findAll();
	
	public Page<Producto> findAll(Pageable pageable);
	
	public Optional<Producto> findById(Long id);
	
	public Producto save(Producto entity);
	
	public void delete(Long id);
	
	public List<Producto> findByNombre (String term);
	
	public List<Producto> findByCategoria (String term);


}
