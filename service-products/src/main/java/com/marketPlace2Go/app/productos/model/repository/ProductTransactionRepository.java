package com.marketPlace2Go.app.productos.model.repository;

import com.marketPlace2Go.app.productos.model.entity.ProductTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ProductTransactionRepository  extends JpaRepository<ProductTransaction,Long> {

  public List<ProductTransaction> findByOrderId(Long id);

}
