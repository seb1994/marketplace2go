package com.marketPlace2Go.app.productos.model.entity;

import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.StockStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long orderId;
    private Long productId;
    private Integer cantidad;
    private Long precio;
    @Enumerated(EnumType.STRING)
    private OrderStatus estado;
    @Enumerated(EnumType.STRING)
    private StockStatus stockStatus;
}
