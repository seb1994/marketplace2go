package com.marketPlace2Go.app.productos.services;

import com.marketPlace2Go.app.productos.model.entity.ProductTransaction;
import com.marketPlace2Go.app.productos.model.repository.ProductTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductoTransSrvImpl implements ProductoTransactionSrv {

	@Autowired
	private ProductTransactionRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<ProductTransaction> findAll() {
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<ProductTransaction> findAll(Pageable pageable) {
		return repository.findAll(pageable);		
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<ProductTransaction> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public ProductTransaction save(ProductTransaction entity) {
		return repository.save(entity);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);		
	}

	public List<ProductTransaction> findByOrderId(Long id){
		return repository.findByOrderId(id);
	}

}
