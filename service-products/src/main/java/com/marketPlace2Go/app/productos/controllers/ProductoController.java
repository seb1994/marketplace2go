package com.marketPlace2Go.app.productos.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.marketPlace2Go.app.productos.model.entity.Producto;
import com.marketPlace2Go.app.productos.services.ProductoSrv;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductoController {

	@Autowired
	private ProductoSrv service; 
	
	@GetMapping("/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id){
		Optional <Producto> opt = service.findById(id);
		
		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(opt.get());
	}
	
	@GetMapping
	public ResponseEntity<?> listar(){
		return ResponseEntity.ok().body(service.findAll());
	}
	
	@GetMapping("/pagina")
	public ResponseEntity<?> listar(Pageable pageable){
		return ResponseEntity.ok().body(service.findAll(pageable));
	}
	
	@PostMapping()
	public ResponseEntity<?> crear(@Valid @RequestBody Producto entity, BindingResult result){
		
		if(result.hasErrors()) {
			return this.validar(result);
		}
		
		Producto entityDB = service.save(entity);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(entityDB);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id){
		Optional <Producto> opt = service.findById(id);
		
		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		service.delete(id);
		
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> editarProducto(@Valid @RequestBody Producto producto, BindingResult result, @PathVariable Long id){
		
		if(result.hasErrors()) {
			return this.validar(result);
		}
		
		Optional <Producto> opt = this.service.findById(id);
		
		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Producto productoDB = opt.get();
		productoDB.setNombre(producto.getNombre());
		productoDB.setPrecio(producto.getPrecio());
		productoDB.setCantidad(producto.getCantidad());
		productoDB.setDescripcion(producto.getDescripcion());
		productoDB.setCategoria(producto.getCategoria());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(productoDB));
	}
	
	@GetMapping("/filtrar/nombre/{term}")
	public ResponseEntity<?> filtrar(@PathVariable String term){
		return ResponseEntity.ok(service.findByNombre(term.toUpperCase()));
	}
	
	@GetMapping("/filtrar/categoria/{term}")
	public ResponseEntity<?> filtrarCategoria(@PathVariable String term){
		log.info("Get Request filtrarCategoria: "+term);
		return ResponseEntity.ok(service.findByCategoria(term.toUpperCase()));
	}
	
	@PostMapping("/crear-foto")
	public ResponseEntity<?> crearFoto(@Valid Producto producto, BindingResult result, @RequestParam MultipartFile archivo) throws IOException {
		if(!archivo.isEmpty()) {
			producto.setImagen(archivo.getBytes());
		}
		
		return this.crear(producto, result);
	}
	
	@GetMapping("/uploads/img/{id}")
	public ResponseEntity<?> verFotoProducto(@PathVariable Long id){
		Optional <Producto> opt = this.service.findById(id);
		
		if(opt.isEmpty() || opt.get().getImagen() == null	) {
			return ResponseEntity.notFound().build();
		}
		
		Resource imagen = new ByteArrayResource(opt.get().getImagen());
		
		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(imagen);
		
	}
	
	@PutMapping("/editar-foto/{id}")
	public ResponseEntity<?> editarFoto(@Valid Producto producto, BindingResult result, @PathVariable Long id, @RequestParam MultipartFile archivo) throws IOException{
		
		Optional <Producto> opt = this.service.findById(id);
		
		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Producto productoDB = opt.get();
		productoDB.setNombre(producto.getNombre());
		productoDB.setPrecio(producto.getPrecio());
		productoDB.setCantidad(producto.getCantidad());
		productoDB.setDescripcion(producto.getDescripcion());
		productoDB.setCategoria(producto.getCategoria());
		
		if(!archivo.isEmpty()) {
			productoDB.setImagen(archivo.getBytes());
		}
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(productoDB));
	}
	
	private ResponseEntity<?> validar(BindingResult result){
		Map <String, Object> errores = new HashMap<String, Object>();
		
		result.getFieldErrors().forEach(err -> {
			errores.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
		});
		
		return ResponseEntity.badRequest().body(errores);
	}
	
}
