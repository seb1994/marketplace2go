package com.marketPlace2Go.app.productos.model.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.marketPlace2Go.app.productos.model.entity.Producto;

public interface ProductoRepository extends PagingAndSortingRepository<Producto, Long> {

	@Query("select pr from Producto pr where upper(pr.nombre) like %?1%")
	public List<Producto> findByNombre (String term);
	
	@Query("select pr from Producto pr where upper(pr.categoria) like %?1%")
	public List<Producto> findByCategoria (String term);
	
}
