package com.marketPlace2Go.app.productos.services;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.marketPlace2Go.app.productos.model.entity.Producto;
import com.marketPlace2Go.app.productos.model.repository.ProductoRepository;

@Service
public class ProductoSrvImpl implements ProductoSrv {

	@Autowired
	private ProductoRepository repository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Producto> findAll() {
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Producto> findAll(Pageable pageable) {
		return repository.findAll(pageable);		
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Producto> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Producto save(Producto entity) {
		return repository.save(entity);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);		
	}

	@Override
	@Transactional(readOnly = true)
	public List<Producto> findByNombre(String term) {
		return repository.findByNombre(term);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Producto> findByCategoria(String term) {
		return repository.findByCategoria(term);
	}

}
