package com.marketPlace2Go.app.productos.services;

import com.marketplacego.service.commons.dto.ProductRequestDto;
import com.marketplacego.service.commons.event.ProductEvent;
import com.marketplacego.service.commons.status.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Sinks;

@Service
public class ProductStatusPublisher {

    @Autowired
    private Sinks.Many<ProductEvent> productSinks;

    public void publishProductEvent(ProductRequestDto productRequestDto, OrderStatus orderStatus){
        ProductEvent productEvent=new ProductEvent(productRequestDto,orderStatus);
        productSinks.tryEmitNext(productEvent);
    }
}
