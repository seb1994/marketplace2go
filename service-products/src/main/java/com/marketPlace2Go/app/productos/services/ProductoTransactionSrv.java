package com.marketPlace2Go.app.productos.services;

import com.marketPlace2Go.app.productos.model.entity.ProductTransaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface ProductoTransactionSrv {
	
    public Iterable<ProductTransaction> findAll();
	
	public Page<ProductTransaction> findAll(Pageable pageable);
	
	public Optional<ProductTransaction> findById(Long id);
	
	public ProductTransaction save(ProductTransaction entity);
	
	public void delete(Long id);

	public List<ProductTransaction> findByOrderId(Long id);

}
