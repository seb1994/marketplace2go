package com.marketPlace2Go.app.productos.config;


import com.marketPlace2Go.app.productos.services.ProductService;
import com.marketPlace2Go.app.productos.services.ProductoSrv;
import com.marketplacego.service.commons.event.OrderEvent;
import com.marketplacego.service.commons.event.PaymentEvent;
import com.marketplacego.service.commons.event.ProductEvent;
import com.marketplacego.service.commons.status.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;
import java.util.function.Function;

@Configuration
public class ProductConsumerConfig {

    @Autowired
    private ProductService productService;

    @Autowired
    ProductUpdateHandler handler;

    @Autowired
    ProductoSrv productoSrv;

    @Bean
    public Function<Flux<OrderEvent>, Flux<ProductEvent>> productProcessor() {
        return orderEventFlux -> orderEventFlux.flatMap(this::processProduct);
    }

    private Mono<ProductEvent> processProduct(OrderEvent orderEvent) {

        if(OrderStatus.ORDER_CREATED.equals(orderEvent.getOrderStatus())){
            return  Mono.fromSupplier(()->this.productService.newOrderEvent(orderEvent));
        }else{
            return Mono.fromRunnable(()->this.productService.cancelOrderEvent(orderEvent));
        }
    }

    @Bean
    public Consumer<PaymentEvent> paymentEventConsumer(){
        return (payment)-> handler.updateProduct(payment.getPaymentRequestDto().getOrderId()
                ,payment.getPaymentStatus(),po->{
            po.setEstado(OrderStatus.ORDER_PAYMENTS_PROBLEMS);

        });
    }
}
