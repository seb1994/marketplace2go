package com.marketPlace2Go.app.productos.config;

import com.marketPlace2Go.app.productos.model.entity.ProductTransaction;
import com.marketPlace2Go.app.productos.model.entity.Producto;
import com.marketPlace2Go.app.productos.model.repository.ProductTransactionRepository;
import com.marketPlace2Go.app.productos.model.repository.ProductoRepository;
import com.marketPlace2Go.app.productos.services.ProductStatusPublisher;
import com.marketplacego.service.commons.dto.ProductRequestDto;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.PaymentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Configuration
public class ProductUpdateHandler {

    @Autowired
    private ProductTransactionRepository repository;

    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private ProductStatusPublisher publisher;

    @Transactional
    public void updateProduct(Long id, PaymentStatus paymentStatus, Consumer<ProductTransaction> consumer) {

            productUpdate( id , paymentStatus);

    }

    private void productUpdate( Long id , PaymentStatus paymentStatus) {

        List<ProductTransaction> list = repository.findByOrderId(id);

        for (ProductTransaction p: list) {
            Optional<Producto> producto = productoRepository.findById(p.getProductId());
            producto.get().setCantidad(producto.get().getCantidad() + p.getCantidad());
            if(paymentStatus.equals(PaymentStatus.PAYMENT_COMPLETED))
                p.setEstado(OrderStatus.ORDER_COMPLETED);
            if(paymentStatus.equals(PaymentStatus.PAYMENT_FAILED))
                p.setEstado(OrderStatus.ORDER_PAYMENTS_PROBLEMS);
            productoRepository.save(producto.get());
            repository.save(p);
        }
         
    }

    public ProductRequestDto convertEntityToDto(ProductTransaction productTransaction) {
        ProductRequestDto productRequestDto = new ProductRequestDto();
        productRequestDto.setOrderId(productTransaction.getOrderId());
        productRequestDto.setProductId(productTransaction.getProductId());
        productRequestDto.setCantidad(productTransaction.getCantidad());
        productRequestDto.setPrecio(productTransaction.getPrecio());
        return productRequestDto;
    }
}
