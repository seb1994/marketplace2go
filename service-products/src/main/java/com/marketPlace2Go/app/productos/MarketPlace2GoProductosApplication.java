package com.marketPlace2Go.app.productos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MarketPlace2GoProductosApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketPlace2GoProductosApplication.class, args);
	}

}
