package com.marketplacego.oauth.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


import com.marketplacego.commons.users.models.entity.Usuario;

@FeignClient(name="service-users")
public interface UsuarioFeignClient {

	@GetMapping("/users/search/search-username")
	public Usuario findByUsername(@RequestParam String username);
	
	@PutMapping("/users/{id}")
	public Usuario update(@RequestBody Usuario usuario, @PathVariable Long id);
}
