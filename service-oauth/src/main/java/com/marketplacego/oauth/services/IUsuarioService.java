package com.marketplacego.oauth.services;

import com.marketplacego.commons.users.models.entity.Usuario;

public interface IUsuarioService {
	
	public Usuario findByUsername(String username);
	
	public Usuario update(Usuario usuario, Long id);

}
