package com.marketplacego.gateway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@EnableWebFluxSecurity
public class SpringSecurityConfig {
	
	@Autowired
	private JwtAuthenticationFilter authenticationFilter;

	@Bean
	public SecurityWebFilterChain configure(ServerHttpSecurity http) {
		return http.authorizeExchange()
				.pathMatchers("/api/security/oauth/**").permitAll()
				.pathMatchers(HttpMethod.GET,

						"/api/users/users","/api/products/products","/api/Customer/api/Customer",
						"/api/orders/orders" ,"/api/payments/payments").permitAll()
				.pathMatchers(HttpMethod.GET, "/api/users/users/{id}", "/api/products/products/{id}",
						"/api/orders/orders/{id}", "/api/payments/payments/{id}","/api/payments/payments/order/{id}").hasAnyRole("ADMIN", "USER")
				.pathMatchers(HttpMethod.POST,"/api/users/users").hasRole("ADMIN")
				.pathMatchers(HttpMethod.POST, "/api/products/products").hasRole("ADMIN")
				.pathMatchers(HttpMethod.POST, "/api/Customer/api/Customer").hasRole("ADMIN")
				.pathMatchers(HttpMethod.POST, "/api/orders/orders").hasRole("ADMIN")
				.pathMatchers(HttpMethod.POST, "/api/payments/payments").hasRole("ADMIN")
				.pathMatchers(HttpMethod.PUT,"/api/users/users/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.PUT, "/api/products/products/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.PUT, "/api/Customer/api/Customer/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.PUT, "/api/orders/orders/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.PUT, "/api/payments/payments/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.DELETE, "/api/users/users/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.DELETE, "/api/products/products/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.DELETE, "/api/Customer/api/Customer/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.DELETE, "/api/orders/orders/{id}").hasRole("ADMIN")
				.pathMatchers(HttpMethod.DELETE, "/api/payments/payments/{id}").hasRole("ADMIN")
				.anyExchange().authenticated()
				.and().addFilterAt(authenticationFilter, SecurityWebFiltersOrder.AUTHENTICATION)
				.csrf().disable()
				.build();
	}
	
}
