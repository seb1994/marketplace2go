package com.marketplace.app.payments.services;

import java.util.Optional;

import com.marketplace.app.payments.model.entity.Payment;
import com.marketplacego.service.commons.event.OrderEvent;
import com.marketplacego.service.commons.event.PaymentEvent;

public interface PaymentSrv {

	public Iterable<Payment> findAll();

	public Optional<Payment> findById(Long id);

	public Payment save(Payment entity);

	public void delete(Long id);

	public Optional<Payment> findByIdOrder(Long id);

	public Payment updatePayment(Payment payment);

}
