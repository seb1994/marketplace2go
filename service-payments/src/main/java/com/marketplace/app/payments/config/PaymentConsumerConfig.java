package com.marketplace.app.payments.config;


import com.marketplace.app.payments.services.PaymentService;
import com.marketplacego.service.commons.event.OrderEvent;
import com.marketplacego.service.commons.event.PaymentEvent;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.PaymentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@Configuration
public class PaymentConsumerConfig {

    @Autowired
    private PaymentService paymentService;

    @Bean
    public Function<Flux<OrderEvent>, Flux<PaymentEvent>> paymentEventConsumer() {

        return orderEventFlux -> orderEventFlux.flatMap(this::paymentEventConsumer);
    }

    private Mono<PaymentEvent> paymentEventConsumer(OrderEvent orderEvent) {


        if (OrderStatus.ORDER_CREATED.equals(orderEvent.getOrderStatus())) {
            return Mono.fromSupplier(() -> this.paymentService.newOrderEvent(orderEvent));
        } else {
            return Mono.fromRunnable(() -> this.paymentService.cancelOrderEvent(orderEvent));
        }

    }

    @Bean
    public Function<Flux<PaymentEvent>, Flux<PaymentEvent>> paymentPutProcessor() {
        return orderEventFlux -> orderEventFlux.flatMap(this::processPutPayment);
    }

    private Mono<PaymentEvent> processPutPayment(PaymentEvent paymentEvent) {

        if (PaymentStatus.PAYMENT_COMPLETED.equals(paymentEvent.getPaymentStatus())) {
            return Mono.fromSupplier(() -> this.paymentService.newPaymentEvent(paymentEvent));
        } else {
            return Mono.fromRunnable(() -> this.paymentService.cancelPaymentEvent(paymentEvent));
        }

    }

}