package com.marketplace.app.payments.services;


import com.marketplace.app.payments.model.entity.Payment;
import com.marketplace.app.payments.repository.CardBalanceRepository;
import com.marketplace.app.payments.repository.PaymentRepository;
import com.marketplace.app.payments.repository.UserTransactionRepository;
import com.marketplacego.service.commons.dto.OrderRequestDto;
import com.marketplacego.service.commons.dto.PaymentRequestDto;
import com.marketplacego.service.commons.event.OrderEvent;
import com.marketplacego.service.commons.event.PaymentEvent;
import com.marketplacego.service.commons.status.PaymentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PaymentService {

	@Autowired
	private UserTransactionRepository userTransactionRepository;

	@Autowired
	private CardBalanceRepository cardBalanceRepository;

	@Autowired
	private PaymentRepository paymentRepository;

	@Transactional
	public PaymentEvent newOrderEvent(OrderEvent orderEvent){
		Payment payment = new Payment();
		try{
		Thread.sleep(2000);
		OrderRequestDto orderRequestDto = orderEvent.getOrderRequestDto();

		PaymentRequestDto paymentRequestDto = new PaymentRequestDto(orderRequestDto.getId(),
				                               orderRequestDto.getTotalPago());

			payment = convertDtoToEntity(orderRequestDto);
			paymentRepository.save(payment);


			return new PaymentEvent(paymentRequestDto, payment.getEstadoPago());
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}


	@Transactional
	public PaymentEvent newPaymentEvent(PaymentEvent paymentEvent){
		Payment payment = new Payment();
		try{
			Thread.sleep(2000);
			PaymentRequestDto paymentRequestDto = paymentEvent.getPaymentRequestDto();

			return new PaymentEvent(paymentRequestDto, payment.getEstadoPago());
		}catch(Exception e){
			e.printStackTrace();
		}

		return null;
	}

    @Transactional
	public void cancelOrderEvent(OrderEvent orderEvent) {

		userTransactionRepository.findById(orderEvent.getOrderRequestDto().getId()).ifPresent(ut -> {
			userTransactionRepository.delete(ut);
			userTransactionRepository.findById(ut.getUserId())
					.ifPresent(ub -> ub.setAmount(ub.getAmount() + ut.getAmount()));
		});
	}

	@Transactional
	public void cancelPaymentEvent(PaymentEvent paymentEvent) {

		userTransactionRepository.findById(paymentEvent.getPaymentRequestDto().getOrderId()).ifPresent(ut -> {
			userTransactionRepository.delete(ut);
			userTransactionRepository.findById(ut.getUserId())
					.ifPresent(ub -> ub.setAmount(ub.getAmount() + ut.getAmount()));
		});
	}


	private Payment convertDtoToEntity(OrderRequestDto dto) {
		Payment payment = new Payment();
		payment.setIdOrder(dto.getId());
		payment.setTotalAmount(dto.getTotalPago().intValue());
        payment.setEstadoPago(PaymentStatus.PAYMENT_PENDNIG);
		return payment;
	}
}
