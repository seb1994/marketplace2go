package com.marketplace.app.payments.services;



import com.marketplacego.service.commons.dto.PaymentRequestDto;
import com.marketplacego.service.commons.event.OrderEvent;
import com.marketplacego.service.commons.event.PaymentEvent;
import com.marketplacego.service.commons.status.PaymentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Sinks;

@Service
public class PaymentStatusPublisher {

    @Autowired
    private Sinks.Many<PaymentEvent> paymentSinks;


    public void publishPaymentEvent(PaymentRequestDto paymentRequestDto, PaymentStatus paymentStatus){
        PaymentEvent paymentEvent = new PaymentEvent(paymentRequestDto,paymentStatus);
        paymentSinks.tryEmitNext(paymentEvent);
    }
}
