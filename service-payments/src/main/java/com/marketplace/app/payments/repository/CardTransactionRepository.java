package com.marketplace.app.payments.repository;

import com.marketplace.app.payments.model.entity.CardBalance;
import com.marketplace.app.payments.model.entity.CardTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardTransactionRepository extends JpaRepository<CardTransaction,Integer>  {
}
