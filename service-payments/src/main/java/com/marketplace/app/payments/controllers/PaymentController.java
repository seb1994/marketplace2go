package com.marketplace.app.payments.controllers;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.marketplace.app.payments.model.entity.Payment;
import com.marketplace.app.payments.services.PaymentSrv;


@RestController
@RequestMapping("/payments")
public class PaymentController {

	@Autowired
	private PaymentSrv service;

	@GetMapping("/{id}")
	@Transactional(readOnly = true)
	public ResponseEntity<?> ver(@PathVariable Long id){
		Optional <Payment> opt = service.findById(id);
		
		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(opt.get());
	}

	@GetMapping
	public ResponseEntity<?> listar(){
		return ResponseEntity.ok().body(service.findAll());
	}
	
	@PostMapping()
	public ResponseEntity<?> crear(@RequestBody Payment entity, BindingResult result){
		Payment entityDB = service.save(entity);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(entityDB);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id){
		Optional <Payment> opt = service.findById(id);
		
		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		service.delete(id);
		
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePayment(@RequestBody Payment payment, BindingResult result, @PathVariable Long id){
		Optional <Payment> opt = this.service.findById(id);
		
		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Payment currentPayment=service.updatePayment(payment);
		return ResponseEntity.status(HttpStatus.CREATED).body(currentPayment);
	}

	@GetMapping("/order/{id}")
	public ResponseEntity<?> verPagoPorIdOrden(@PathVariable Long id){
		Optional <Payment> opt = service.findByIdOrder(id);

		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(opt.get());
	}
	
}
