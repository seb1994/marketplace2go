package com.marketplace.app.payments.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

import com.marketplace.app.payments.model.entity.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    public Optional<Payment> findByIdOrder(Long id);

}
