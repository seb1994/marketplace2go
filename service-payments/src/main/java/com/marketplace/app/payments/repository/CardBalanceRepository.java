package com.marketplace.app.payments.repository;

import com.marketplace.app.payments.model.entity.CardBalance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardBalanceRepository extends JpaRepository<CardBalance,Integer>       {}


