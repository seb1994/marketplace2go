package com.marketplace.app.payments.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CardTransaction {

    @Id
    private Integer orderId;
    private String cardNumber;
    private int amount;
}
