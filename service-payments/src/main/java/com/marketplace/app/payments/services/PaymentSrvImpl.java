package com.marketplace.app.payments.services;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.marketplace.app.payments.model.entity.CardBalance;
import com.marketplace.app.payments.model.entity.CardTransaction;
import com.marketplace.app.payments.repository.CardBalanceRepository;
import com.marketplace.app.payments.repository.CardTransactionRepository;
import com.marketplacego.service.commons.dto.OrderRequestDto;
import com.marketplacego.service.commons.dto.PaymentRequestDto;
import com.marketplacego.service.commons.event.OrderEvent;
import com.marketplacego.service.commons.event.PaymentEvent;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.PaymentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marketplace.app.payments.model.entity.Payment;
import com.marketplace.app.payments.repository.PaymentRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service
public class PaymentSrvImpl implements PaymentSrv {

	@Autowired
	private PaymentRepository paymentRepository;

	@Autowired
	private CardBalanceRepository cardBalanceRepository;

	@Autowired
	private CardTransactionRepository cardTransactionRepository;


	@Autowired
	private PaymentStatusPublisher paymentStatusPublisher;
	
	@Override
	public Iterable<Payment> findAll() {
		return paymentRepository.findAll();
	}

	@Override
	public Optional<Payment> findById(Long id) {
		return paymentRepository.findById(id);
	}

	@Override
	public Payment save(Payment entity) {
		return paymentRepository.save(entity);
	}

	@Override
	public void delete(Long id) {
		paymentRepository.deleteById(id);		
	}

	@Override
    public Optional<Payment> findByIdOrder(Long id) {
		return paymentRepository.findByIdOrder(id) ;
	}

	@PostConstruct
	public void initUserBalanceInDB() {
		cardBalanceRepository.saveAll(Stream.of(new CardBalance(101, 100000),
						new CardBalance(145100, 5000000),
						new CardBalance(145101, 2000000),
						new CardBalance(145102, 150000),
						new CardBalance(145103, 100000),
						new CardBalance(145104, 50000),
						new CardBalance(145105, 300000),
						new CardBalance(145106, 80000),
						new CardBalance(145107, 120000),
						new CardBalance(145108, 70000),
						new CardBalance(145109, 30000),
						new CardBalance(145110, 1500000),
						new CardBalance(145111, 50000)).collect(Collectors.toList()));
	}

	@Override
	public Payment updatePayment(Payment payment){
		Optional<CardBalance> cardBalanceRepositoryDB = cardBalanceRepository.findById(Integer.parseInt(payment.getCreditCardNumber()));

		if(cardBalanceRepositoryDB.get().getAmount() > payment.getTotalAmount()){
			cardBalanceRepositoryDB.get().setAmount(cardBalanceRepositoryDB.get().getAmount() - payment.getTotalAmount() );
			cardTransactionRepository.save(new CardTransaction(payment.getIdOrder().intValue(),
					payment.getCreditCardNumber(), payment.getTotalAmount()));
			payment.setEstadoPago(PaymentStatus.PAYMENT_COMPLETED);
		}else{
			payment.setEstadoPago(PaymentStatus.PAYMENT_FAILED);
		}

		paymentRepository.save(payment);

		paymentStatusPublisher.publishPaymentEvent(convertDtoToEntity(payment),payment.getEstadoPago());

		return payment;

	}

	private PaymentRequestDto convertDtoToEntity(Payment payment) {
		PaymentRequestDto paymentRequestDto = new PaymentRequestDto();

		paymentRequestDto.setAmount(payment.getTotalAmount().longValue());
		paymentRequestDto.setOrderId(payment.getIdOrder());
		return paymentRequestDto;
	}
}
