package com.marketplace.app.payments.model.entity;

import java.util.Date;

import com.marketplacego.service.commons.status.PaymentStatus;
import com.marketplacego.service.commons.status.StockStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="payments")
public class Payment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long paymentId;

	@Temporal(TemporalType.DATE)
	private Date paymentDate;
	
	private String paymentMethod;
	
	private String creditCardNumber;
	
	private String creditCardFranchise;

	private String creditCardCVC;
	
	private Long idOrder;
	
	private Integer totalAmount;
	
	private String numberDues;

	@Enumerated(EnumType.STRING)
	private PaymentStatus estadoPago;

	@Enumerated(EnumType.STRING)
	private StockStatus stockStatus;
	
	private String numberPaymentReference;

	@PrePersist
	public void prePersist() {
		this.paymentDate = new Date();
	}


	@Override
	public boolean equals(Object obj) {
		
		if(this == obj) {
			return true;
		}
		
		if(!(obj instanceof Payment)) {
			return false;
		}
		
		Payment payment = (Payment) obj;
		
		return this.paymentId != null && this.paymentId.equals(payment.getPaymentId());
	}
	
}
