package com.marketplacego.service.commons.status;

public enum ShippingStatus {

    SHIPPING_COMPLETED,SHIPPING_FAILED,SHIPPING_TEST
}
