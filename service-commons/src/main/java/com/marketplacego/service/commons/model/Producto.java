package com.marketplacego.service.commons.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class Producto {

    private Long id;
    private String nombre;
    private Long precio;
    private Integer cantidad;
    private String descripcion;
    private String categoria;
    private byte[] imagen;
    private Date fecCreacion;

}
