package com.marketplacego.service.commons.dto;

import com.marketplacego.service.commons.model.PurchaseOrderDetailDto;
import com.marketplacego.service.commons.model.Usuario;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.PaymentStatus;
import com.marketplacego.service.commons.status.StockStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequestDto {

    private Long id;
    private Long idClient;
    private Date fechaOrden;
    private Date fechaEstadoActualOrden;
    private Long idPago;
    private Long totalPago;
    private List<PurchaseOrderDetailDto> orderDetails;
    private Usuario usuario;
    private OrderStatus orderStatus;
    private PaymentStatus paymentStatus;
    private StockStatus stockStatus;
}
