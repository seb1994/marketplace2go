package com.marketplacego.service.commons.dto;

import com.marketplacego.service.commons.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponseDto {

    private Long userId;
    private Long productId;
    private Long amount;
    private Long orderId;
    private OrderStatus orderStatus;

}