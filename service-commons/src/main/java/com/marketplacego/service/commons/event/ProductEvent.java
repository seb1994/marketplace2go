package com.marketplacego.service.commons.event;

import com.marketplacego.service.commons.dto.OrderRequestDto;
import com.marketplacego.service.commons.dto.ProductRequestDto;
import com.marketplacego.service.commons.status.OrderStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Data
public class ProductEvent implements com.marketplacego.service.commons.event.Event{

    private UUID eventId=UUID.randomUUID();
    private Date eventDate=new Date();
    private OrderRequestDto orderRequestDto;
    private ProductRequestDto productRequestDto;
    private OrderStatus orderStatus;

    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return eventDate;
    }

    public OrderRequestDto getOrderRequestDto() {
        return orderRequestDto;
    }

    public void setOrderRequestDto(OrderRequestDto orderRequestDto) {
        this.orderRequestDto = orderRequestDto;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setEventId(UUID eventId) {
        this.eventId = eventId;
    }

    public ProductEvent(ProductRequestDto productRequestDto, OrderStatus orderStatus) {
        this.productRequestDto = productRequestDto;
        this.orderStatus = orderStatus;
    }
}
