package com.marketplacego.orders.client;

import com.marketplacego.orders.model.Usuario;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="service-users", fallback = UsuarioHystrixFallbackFactory.class)
public interface UsuarioClient {
    @GetMapping(value="/users/{id}")
    public ResponseEntity<Usuario> getUsuario(@PathVariable(name="id") long id);
}
