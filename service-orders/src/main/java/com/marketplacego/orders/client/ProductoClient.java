package com.marketplacego.orders.client;

import com.marketplacego.orders.model.Producto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "service-products")
@RequestMapping(value = "/products")
public interface ProductoClient {

    @GetMapping(value = "/{id}")
    public ResponseEntity<Producto> getProducto(@PathVariable("id") Long id);

    @PutMapping("/{id}/stock")
    public ResponseEntity<Producto> updateStock(@PathVariable long id,
                                               @RequestParam(name="quantity",required = true) Double quantity);


}