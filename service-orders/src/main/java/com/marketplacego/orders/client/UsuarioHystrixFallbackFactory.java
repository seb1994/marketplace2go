package com.marketplacego.orders.client;

import com.marketplacego.orders.model.Usuario;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UsuarioHystrixFallbackFactory implements UsuarioClient{
    @Override
    public ResponseEntity<Usuario> getUsuario(long id) {
        Usuario usuario = Usuario.builder()
                .nombre("none")
                .apellido("none")
                .email("none").build();

        return ResponseEntity.ok(usuario);


    }
}
