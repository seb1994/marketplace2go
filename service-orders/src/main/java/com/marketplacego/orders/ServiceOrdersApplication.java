package com.marketplacego.orders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrix
public class ServiceOrdersApplication implements CommandLineRunner {

	public static void main(String[] args) {

		SpringApplication.run(ServiceOrdersApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Llego a ServiceOrdersApplication!!!!!!");
	}

}
