package com.marketplacego.orders.model;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class Usuario {
     private Long id;
     private String username;
     private String password;
     private Boolean enabled;
     private String nombre;
     private String apellido;
     private String email;

}
