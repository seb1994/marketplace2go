package com.marketplacego.orders.entity;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import com.marketplacego.orders.model.Producto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Positive(message = "La cantidad debe ser mayor que cero")
    private Double quantity;

    private Double  price;

    @Column(name = "producto_id")
    private Long productId;

    @Transient
    private Double subTotal;

    @Transient
    private Producto producto;

    public Double getSubTotal(){
        if (this.price >0  && this.quantity >0 ){
            return this.quantity * this.price;
        }else {
            return (double) 0;
        }
    }

}
