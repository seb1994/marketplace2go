package com.marketplacego.orders.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.marketplacego.orders.model.Usuario;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.PaymentStatus;
import com.marketplacego.service.commons.status.StockStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
public class PurchaseOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long idClient;

    @Temporal(TemporalType.DATE)
    private Date fechaOrden;

    @Temporal(TemporalType.DATE)
    private Date fechaEstadoActualOrden;


    private Long idPago;

    @NotNull(message = "El total del pago no puede ser nulo")
    private Long totalPago;

    @Valid
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id")
    private List<PurchaseOrderDetail> orderDetails;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;

    @Enumerated(EnumType.STRING)
    private StockStatus stockStatus;

    public PurchaseOrder(){

        orderDetails = new ArrayList<>();
    }


    @Transient
    private Usuario usuario;

    @PrePersist
    public void prePersist() {
        this.fechaOrden = new Date();
        this.fechaEstadoActualOrden = new Date();
    }


}
