package com.marketplacego.orders.config;


import com.marketplacego.orders.entity.PurchaseOrder;
import com.marketplacego.orders.entity.PurchaseOrderDetail;
import com.marketplacego.orders.repository.OrderRepository;

import com.marketplacego.orders.service.OrderStatusPublisher;
import com.marketplacego.service.commons.dto.OrderRequestDto;
import com.marketplacego.service.commons.model.PurchaseOrderDetailDto;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.PaymentStatus;
import com.marketplacego.service.commons.status.StockStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import java.util.function.Consumer;

@Configuration
public class OrderStatusUpdateHandler {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private OrderStatusPublisher publisher;

    @Transactional
    public void updateOrder(Long id, Consumer<PurchaseOrder> consumer) {
        repository.findById(id).ifPresent(consumer.andThen(this::updateOrder));
    }

    private void updateOrder(PurchaseOrder purchaseOrder) {
    	boolean isPaymentComplete = purchaseOrder.getPaymentStatus().equals(PaymentStatus.PAYMENT_COMPLETED);
        if (purchaseOrder.getPaymentStatus().equals(PaymentStatus.PAYMENT_FAILED)) {
            purchaseOrder.setOrderStatus(OrderStatus.ORDER_PAYMENTS_PROBLEMS);
            purchaseOrder.setStockStatus(StockStatus.STOCK_COMPLETED);
        }
        if(isPaymentComplete) {
            purchaseOrder.setOrderStatus(OrderStatus.ORDER_COMPLETED);
            purchaseOrder.setStockStatus(StockStatus.STOCK_COMPLETED);
        }
        repository.save(purchaseOrder);

    }

    public OrderRequestDto convertEntityToDto(PurchaseOrder purchaseOrder) {
        OrderRequestDto orderRequestDto = new OrderRequestDto();
        orderRequestDto.setId(purchaseOrder.getId());

        for (PurchaseOrderDetail purch:purchaseOrder.getOrderDetails()) {
            orderRequestDto.getOrderDetails().add(convert(purch,purchaseOrder.getId()));
        }
        orderRequestDto.setTotalPago(purchaseOrder.getTotalPago());
        orderRequestDto.setPaymentStatus(PaymentStatus.PAYMENT_PENDNIG);

        return orderRequestDto;
    }

    public PurchaseOrderDetailDto convert(PurchaseOrderDetail purchaseOrderDetail, Long purcharseId){

        PurchaseOrderDetailDto purchaseOrderDetailDto = new PurchaseOrderDetailDto();
        purchaseOrderDetailDto.setId(purchaseOrderDetail.getId());
        purchaseOrderDetailDto.setProductId(purchaseOrderDetail.getProductId());
        purchaseOrderDetailDto.setPrice(purchaseOrderDetail.getPrice());
        purchaseOrderDetailDto.setOrder_id(purcharseId);

        return purchaseOrderDetailDto;

    }
}
