package com.marketplacego.orders.repository;

import com.marketplacego.orders.entity.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderRepository  extends JpaRepository<PurchaseOrder,Long> {

}
