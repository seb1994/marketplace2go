package com.marketplacego.orders.repository;

import com.marketplacego.orders.entity.PurchaseOrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailsRepository extends JpaRepository<PurchaseOrderDetail,Long> {
}
