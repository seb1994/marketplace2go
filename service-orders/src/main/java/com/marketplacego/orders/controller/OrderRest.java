package com.marketplacego.orders.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketplacego.orders.entity.PurchaseOrder;
import com.marketplacego.orders.service.OrderService;
import com.marketplacego.service.commons.dto.OrderRequestDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/orders")
public class OrderRest {

    @Autowired
    OrderService orderService;

    @GetMapping
    public ResponseEntity<List<PurchaseOrder>> listAllOrders(){

        List<PurchaseOrder> orders = orderService.findOrderAll();
        if (orders.isEmpty()) {
            return  ResponseEntity.noContent().build();
        }
        return  ResponseEntity.ok(orders);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PurchaseOrder> getInvoice(@PathVariable("id") long id){

        log.info("Fetching Order with id {}", id);
        PurchaseOrder order  = orderService.getOrder(id);
        if (null == order) {
            log.error("Order with id {} not found.", id);
            return  ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok(order);
    }

    @PostMapping
    public ResponseEntity<PurchaseOrder> createOrder(@Valid @RequestBody PurchaseOrder order, BindingResult result){
        log.info("Creating Order : {}", order);
        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));
        }
        OrderRequestDto orderRequestDto = new OrderRequestDto();

        PurchaseOrder invoiceDB = orderService.createOrder (order);

        return  ResponseEntity.status( HttpStatus.CREATED).body(invoiceDB);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PurchaseOrder> updateInvoice(@PathVariable("id") Long id, @RequestBody PurchaseOrder order){
        log.info("Updating Order with id {}", id);

        order.setId(id);
        PurchaseOrder currentOrder=orderService.updateOrder(order);

        if (currentOrder == null) {
            log.error("Unable to update. Order with id {} not found.", id);
            return  ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok(currentOrder);

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PurchaseOrder> deleteInvoice(@PathVariable("id") Long id){
        log.info("Fetching & Deleting Order with id {}", id);

        PurchaseOrder order = orderService.getOrder(id);
        if (order == null) {
            log.error("Unable to delete. Order with id {} not found.", id);
            return  ResponseEntity.notFound().build();
        }
        order = orderService.deleteOrder(order);
        return ResponseEntity.ok(order);

    }

    private String formatMessage( BindingResult result){
        List<Map<String,String>> errors = result.getFieldErrors().stream()
                .map(err ->{
                    Map<String,String> error =  new HashMap<>();
                    error.put(err.getField(), err.getDefaultMessage());
                    return error;

                }).collect(Collectors.toList());
        ErrorMessage errorMessage = ErrorMessage.builder()
                .code("01")
                .messages(errors).build();
        ObjectMapper mapper = new ObjectMapper();

        String jsonString="";
        try {
            jsonString = mapper.writeValueAsString(errorMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

}
