package com.marketplacego.orders.service;

import com.marketplacego.orders.entity.PurchaseOrder;

import java.util.List;

public interface OrderService {

    public List<PurchaseOrder> findOrderAll();

    public PurchaseOrder  createOrder(PurchaseOrder order);
    public PurchaseOrder updateOrder(PurchaseOrder order);
    public PurchaseOrder deleteOrder(PurchaseOrder order);

    public PurchaseOrder getOrder(Long id);
}
