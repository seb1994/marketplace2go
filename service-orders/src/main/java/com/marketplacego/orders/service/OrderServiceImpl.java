package com.marketplacego.orders.service;

import com.marketplacego.orders.client.ProductoClient;
import com.marketplacego.orders.client.UsuarioClient;
import com.marketplacego.orders.entity.PurchaseOrder;
import com.marketplacego.orders.entity.PurchaseOrderDetail;
import com.marketplacego.service.commons.model.PurchaseOrderDetailDto;
import com.marketplacego.orders.model.Producto;
import com.marketplacego.orders.model.Usuario;
import com.marketplacego.orders.repository.OrderDetailsRepository;
import com.marketplacego.orders.repository.OrderRepository;
import com.marketplacego.service.commons.dto.OrderRequestDto;
import com.marketplacego.service.commons.status.OrderStatus;
import com.marketplacego.service.commons.status.PaymentStatus;
import com.marketplacego.service.commons.status.StockStatus;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Data
public class OrderServiceImpl implements OrderService{

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderDetailsRepository  orderDetailsRepository;

    @Autowired
    UsuarioClient usuarioClient;

    @Autowired
    ProductoClient productClient;

    @Autowired
    OrderStatusPublisher orderStatusPublisher;

    @Override
    public List<PurchaseOrder> findOrderAll() {
        List<PurchaseOrder> orderList  = orderRepository.findAll();

        if (null != orderList) {
            orderList.forEach(order -> {
                Usuario usuario = usuarioClient.getUsuario(order.getIdClient()).getBody();
                order.setUsuario(usuario);
                List<PurchaseOrderDetail> listItem = order.getOrderDetails();
                listItem.forEach(orderItem -> {
                            Producto producto = productClient.getProducto(orderItem.getProductId()).getBody();
                            orderItem.setProducto(producto);

                        }
                );

            });

        }

        return orderList;
    }

    @Override
    public PurchaseOrder createOrder(PurchaseOrder order) {
        OrderRequestDto orderRequestDto = new OrderRequestDto();
        log.info("Llego a create purchaseOrder---->" + order);

        order.setPaymentStatus(PaymentStatus.PAYMENT_PENDNIG);
        order.setStockStatus(StockStatus.STOCK_PENDNIG);
        order.setOrderStatus(OrderStatus.ORDER_CREATED);

        order = orderRepository.save(order);

        orderRequestDto = convertDtoToEntity(order);

        for( PurchaseOrderDetailDto purchaseOrderDetailDto :orderRequestDto.getOrderDetails()){
            System.out.println("Lista enviada a productos: " + purchaseOrderDetailDto.getId() + " - "+ purchaseOrderDetailDto.getProductId());
        }

        orderStatusPublisher.publishOrderEvent(orderRequestDto, OrderStatus.ORDER_CREATED);

        log.info("Actualizara Stock...");

        return order;
    }

    @Override
    public PurchaseOrder updateOrder(PurchaseOrder purchaseOrder) {
        PurchaseOrder orderDB = getOrder(purchaseOrder.getId());
        if (orderDB == null){
            return  null;
        }

        return orderRepository.save(purchaseOrder);
    }

    @Override
    public PurchaseOrder deleteOrder(PurchaseOrder purchaseOrder) {
        PurchaseOrder orderDB = getOrder(purchaseOrder.getId());
        if (orderDB == null){
            return  null;
        }

        return orderRepository.save(orderDB);
    }

    @Override
    public PurchaseOrder getOrder(Long id) {
        PurchaseOrder purchaseOrder = orderRepository.findById(id).orElse(null);

        if(null != purchaseOrder ) {
            Usuario usuario = usuarioClient.getUsuario(purchaseOrder.getIdClient()).getBody();
            purchaseOrder.setUsuario(usuario);

            List<PurchaseOrderDetail> purchaseOrderDetailList = purchaseOrder.getOrderDetails().stream().map(orderItem -> {
                Producto producto = productClient.getProducto(orderItem.getProductId()).getBody();
                orderItem.setProducto(producto);
                return orderItem;
            }).collect(Collectors.toList());

            purchaseOrder.setOrderDetails(purchaseOrderDetailList);
        }

        return purchaseOrder;
    }

    private OrderRequestDto convertDtoToEntity(PurchaseOrder purchaseOrder) {

        List<PurchaseOrderDetail> purchaseOrdersDto = purchaseOrder.getOrderDetails();
        PurchaseOrderDetailDto purchaseOrderDetailDto = new PurchaseOrderDetailDto() ;
        OrderRequestDto orderDto = new OrderRequestDto();
        orderDto.setFechaEstadoActualOrden(purchaseOrder.getFechaEstadoActualOrden());
        orderDto.setFechaOrden(purchaseOrder.getFechaOrden());
        orderDto.setIdPago(purchaseOrder.getIdPago());
        orderDto.setIdClient(purchaseOrder.getIdClient());
        orderDto.setTotalPago(purchaseOrder.getTotalPago());
        orderDto.setId(purchaseOrder.getId());
        orderDto.setStockStatus(purchaseOrder.getStockStatus());
        orderDto.setOrderDetails(new ArrayList<PurchaseOrderDetailDto>());

        for(PurchaseOrderDetail orderDetail:purchaseOrdersDto){
            purchaseOrderDetailDto = new PurchaseOrderDetailDto() ;
            purchaseOrderDetailDto = convertDtoToEntityDetail(orderDetail);
            orderDto.getOrderDetails().add(purchaseOrderDetailDto);
        }

        return orderDto;
    }

    private PurchaseOrderDetailDto convertDtoToEntityDetail(PurchaseOrderDetail purchaseOrderDetail) {

        PurchaseOrderDetailDto purchaseOrderDetailDto = new PurchaseOrderDetailDto() ;
        purchaseOrderDetailDto.setId(purchaseOrderDetail.getId());
        purchaseOrderDetailDto.setProductId(purchaseOrderDetail.getProductId());
        purchaseOrderDetailDto.setQuantity(purchaseOrderDetail.getQuantity());
        purchaseOrderDetailDto.setPrice(purchaseOrderDetail.getPrice());

        return purchaseOrderDetailDto;
    }
}
